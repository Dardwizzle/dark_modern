# Dark Modern Theme

This is for the [ALT-F](https://sourceforge.net/p/alt-f/) Firmware

This file uses:

* [Vertical-responsive-menu](https://github.com/cbfranca/vertical-responsive-menu)
* [Awesome fonts](http://fontawesome.io/icons)
* [Google Webfonts](https://fonts.google.com)

Started a discussion on [ALT-F](https://groups.google.com/forum/#!topic/alt-f/Lzt4hzTu-X8) Discussion group.

Theme is built for firmware 1.0

## Roadmap

Plans ahead for this theme

* Split functions up to  separate .css

## Screenshots

<a href="http://www.imagebam.com/image/2333d3698356893" target="_blank"><img src="http://thumbs2.imagebam.com/d3/05/f9/2333d3698356893.jpg" alt="imagebam.com"></a> <a href="http://www.imagebam.com/image/2a03ad698356913" target="_blank"><img src="http://thumbs2.imagebam.com/79/3a/1e/2a03ad698356913.jpg" alt="imagebam.com"></a> <a href="http://www.imagebam.com/image/c00bfa698364323" target="_blank"><img src="http://thumbs2.imagebam.com/38/32/84/c00bfa698364323.jpg" alt="imagebam.com"></a> 